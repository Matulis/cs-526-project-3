
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>

#include "vtkSmartPointer.h"
#include "vtkStructuredPointsReader.h"
#include "vtkMarchingCubes.h"
#include "vtkPolyDataConnectivityFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkMetaImageReader.h"

#include "vtkImageActor.h"
#include "vtkImageMapper3D.h"

//For clipping
#include <vtkPlane.h>
#include <vtkClipPolyData.h>

#include <omega.h>
#include <omegaToolkit.h>
#include <omegaVtk/omegaVtk.h>


#include <map>

using namespace omega;
using namespace omegaToolkit;
using namespace omegaToolkit::ui;
using namespace omegaVtk;
using namespace omicron;


//Class to represent a volume and contain its corresponding menu actions
// transparency, clipping, contour values?

namespace mm {

class Volume : public IMenuItemListener
{
public:
	Volume(double threshold = 50);
	virtual void loadData(std::string filename);
	virtual void onMenuItemEvent(MenuItem *mi);

	void setClippingEnabled(bool enabled);
	bool getClippingEnabled();
	void setColor(double r, double g, double b);

	void addSelfToMenu(Menu * menuToAddTo);

	void updateContour();

	//Sloppy, eventually should be get/set methods to access
	vtkActor * getActor();

	MenuItem * isoSlider;
	MenuItem * opacitySlider;
	MenuItem * imageSlider;
	MenuItem * clippingEnabledCheckbox;

	vtkPlane * clippingPlane;

	std::map<int,double> contourMap;

	vtkImageActor * imageActor;

private:
	void updateClipping();

	vtkMetaImageReader * metaReader;
	vtkMarchingCubes * mc;
	vtkPolyDataConnectivityFilter * confilter;
	vtkPolyDataMapper * mapper;

	
	vtkClipPolyData * clipper;

	vtkActor * actor;

	

	bool isClippingEnabled;

};


}