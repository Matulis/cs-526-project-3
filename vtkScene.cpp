
/**************************************************************************************************
 * THE OMEGA LIB PROJECT
 *-------------------------------------------------------------------------------------------------
 * Copyright 2010-2011		Electronic Visualization Laboratory, University of Illinois at Chicago
 * Authors:										
 *  Alessandro Febretti		febret@gmail.com
 *-------------------------------------------------------------------------------------------------
 * Copyright (c) 2010-2011, Electronic Visualization Laboratory, University of Illinois at Chicago
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted 
 * provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of conditions 
 * and the following disclaimer. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the documentation and/or other 
 * materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************************************/
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>

#include "vtkSmartPointer.h"
#include "vtkStructuredPointsReader.h"
#include "vtkMarchingCubes.h"
#include "vtkPolyDataConnectivityFilter.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkProperty.h"
#include "vtkMetaImageReader.h"

#include "vtkImageActor.h"
#include <vtkImageMapper3D.h>
#include <vtkImageData.h>

//For clipping
#include <vtkPlane.h>
#include <vtkClipPolyData.h>

#include <omega.h>
#include <omegaToolkit.h>
#include <omegaVtk/omegaVtk.h>

#include "Volume.h"

using namespace omega;
using namespace omegaToolkit;
using namespace omegaToolkit::ui;
using namespace omegaVtk;
using namespace mm;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VtkScene: public EngineModule, IMenuItemListener
{
public:
	VtkScene();
	virtual void initialize();
	virtual void onMenuItemEvent(MenuItem* mi);
	virtual void handleEvent(const Event & evt);

private:
	
	VtkModule* myVtkModule;
	SceneNode* mySceneNode;

	SceneNode* myTestNode;
	vtkActor * myActor;
	vtkPolyDataMapper* myPolyDataMapper;
	//DefaultMouseInteractor* myMouseInteractor;
	vtkSphereSource* mySphere;

	Volume * volume;

	bool holdClipped;


	//GUI Controls
	Button * mainButton;
	Menu * volMenu;

	//DefaultMouseInteractor* myMouseInteractor;
	
	// Client objecs.
	//RendererObject<vtkPolyDataMapper*> myPolyDataMapper;
	//RendererObject<vtkActor*> myActor;
};

void VtkScene::handleEvent(const Event & evt)
{
	EngineModule::handleEvent(evt);

	//Handle our scenes events here

	if(evt.isKeyDown('c'))
	{
		std::cout << "C IS PRESSED!!" << std::endl;
		volume->setClippingEnabled(!volume->getClippingEnabled());
	}
	if(evt.isKeyDown('v'))
	{
		std::cout << "V IS PRESSED!!" << std::endl;
		//Hold the clipped value
		holdClipped = !holdClipped;
	}
	if(evt.isKeyDown('y'))
	{
		std::cout << "Y IS PRESSED!!" << std::endl;
		volume->clippingPlane->SetOrigin(0,0,0);
	}
	if(evt.isButtonDown(Event::Button1))
	{
		if(holdClipped == false)
		{
			std::cout << evt.getPosition() << std::endl;
			DisplaySystem * ds = getEngine()->getSystemManager()->getDisplaySystem();
			Ray r;
			ds->getViewRayFromEvent(evt,r);

			Vector3f hitPoint;
			if(mySceneNode->hit(r,&hitPoint,SceneNode::HitBest))
			{
				std::cout << hitPoint << std::endl;
				hitPoint = mySceneNode->getOrientation().conjugate()._transformVector(hitPoint);
				//Not sure why this value is off..(need to negate Z or conjugate but then X is off)

				volume->clippingPlane->SetOrigin(hitPoint[0],hitPoint[1],hitPoint[2]);
				//myTestNode->setPosition(hitPoint[0],hitPoint[1],hitPoint[2]);

				std::cout << "Rotation: " << hitPoint << std::endl;
			
			}
		}

	}
}

void VtkScene::onMenuItemEvent(MenuItem* mi)
{
	if(mi->getButton() == mainButton)
	{
		volMenu->show();
	}

	//// is this event generated by the 'manipulate object' menu item?
	//if(mi == isoSlider)
	//{
	//	int value = mi->getSlider()->getValue();
	//	//mc->SetValue(0,value);
	//	plane->SetOrigin(0,value,0);
	//	cout << value << std::endl;
	//}
}

///////////////////////////////////////////////////////////////////////////////////////////////////
VtkScene::VtkScene(): EngineModule("VtkScene")
{
	holdClipped = false;
	// Create and register the omegalib vtk module.
	myVtkModule = new VtkModule();
	ModuleServices::addModule(myVtkModule);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void VtkScene::initialize()
{
	mySphere = vtkSphereSource::New(); 
	mySphere->SetRadius(0.20); 
	mySphere->SetThetaResolution(18); 
	mySphere->SetPhiResolution(18);

	// Create an omegalib scene node. We will attach our vtk objects to it.


	// Create a mouse interactor and associate it with our scene node.
	//myMouseInteractor = new DefaultMouseInteractor();
	//myMouseInteractor->setSceneNode(mySceneNode);
	//ModuleServices::addModule(myMouseInteractor);

	myPolyDataMapper = vtkPolyDataMapper::New();
	myPolyDataMapper->SetInput(mySphere->GetOutput());
	myActor = vtkActor::New(); 
	myActor->SetMapper(myPolyDataMapper); 
	myActor->GetProperty()->SetColor(0,0,1);

	
    std::string filename = "/Users/mmatulyauskas/omegalib/vtkVolume/build/data/HeadMRVolume.mhd";
    
//    filename = "/Users/mmatulyauskas/omegalib/vtkVolume/build/all256x256_gaussian7.mhd";

	volume = new Volume();
	volume->loadData(filename);


	MenuManager * m_menuManager = MenuManager::createAndInitialize();
    
	Menu * mainMenu = m_menuManager->createMenu("Main Menu");
	m_menuManager->setMainMenu(mainMenu);
    
	UiModule * uim = UiModule::createAndInitialize();
	WidgetFactory * wf = uim->getWidgetFactory();
    
	/*mainButton = wf->createButton("mainButton",uim->getUi());
	mainButton->setText("Main Menu");*/

	vtkMetaImageReader * metaReader = vtkMetaImageReader::New();
	metaReader->SetFileName(filename.c_str());
	metaReader->Update();

	

	volume->imageActor = vtkImageActor::New();
	volume->imageActor->GetMapper()->SetInputConnection(metaReader->GetOutputPort());
	volume->imageActor->SetZSlice(24);

	int x = metaReader->GetWidth();
	int y = metaReader->GetHeight();
    int z = volume->imageActor->GetWholeZMax();
	volume->imageActor->SetDisplayExtent(0,x,0,y,0,0);
	
	volMenu = mainMenu->addSubMenu("Volume");

	volMenu->addLabel("Iso-value:");
	volume->isoSlider = volMenu->addSlider(255,"");
	volume->isoSlider->getSlider()->setValue(volume->contourMap[0]);
	volume->isoSlider->setListener(volume);

	volMenu->addLabel("Opacity");
	volume->opacitySlider = volMenu->addSlider(1000,"");
	volume->opacitySlider->setListener(volume);

	volMenu->addLabel("Image Slice:");
	volume->imageSlider = volMenu->addSlider(z,"");
	volume->imageSlider->setListener(volume);

	volMenu->show();

	//    mainMenu->getContainer()->setPosition(5,25);



	// Create an omegalib scene node. We will attach our vtk objects to it.
	mySceneNode = new SceneNode(getEngine(), "vtkRoot2");
	mySceneNode->setPosition(0, 1,0);
	//mySceneNode->setBoundingBoxVisible(true);
	getEngine()->getScene()->addChild(mySceneNode);
	getEngine()->getDefaultCamera()->getController()->setSpeed(100);

	// Create an omegalib scene node. We will attach our vtk objects to it.
	myTestNode = new SceneNode(getEngine(), "vtkRootTest");
	myTestNode->setPosition(0, 1,0);
	myTestNode->setBoundingBoxVisible(true);
	getEngine()->getScene()->addChild(myTestNode);



	//Correct the volumes orientation(will need to correct for this in the clipping plane)
	mySceneNode->pitch(((M_PI/180)*-90));

	myTestNode->pitch(((M_PI/180)*-90));

	//volume->getActor()->RotateX(-90);
	myVtkModule->attachProp(volume->imageActor,myTestNode);
	myVtkModule->attachProp(volume->getActor(), mySceneNode);

	//myVtkModule->attachProp(myActor, myTestNode);
	// Setup the camera
	getEngine()->getDefaultCamera()->focusOn(getEngine()->getScene());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Application entry point
int main(int argc, char** argv)
{
	Application<VtkScene> app("vtkhello");
	return omain(app, argc, argv);
}
