
#include "Volume.h"


using namespace mm;


Volume::Volume(double threshold):
metaReader(vtkMetaImageReader::New()),
mc(vtkMarchingCubes::New()),
confilter(vtkPolyDataConnectivityFilter::New()),
mapper(vtkPolyDataMapper::New()),
clippingPlane(vtkPlane::New()),
clipper(vtkClipPolyData::New()),
actor(vtkActor::New())
{
	contourMap.insert(std::make_pair<int,double>(0,threshold));

	isClippingEnabled = true;
}

vtkActor * Volume::getActor()
{
	return actor;
}
void Volume::updateContour()
{
	if(mc)
	{
		//Iterate over all contour values
		for(std::map<int,double>::iterator it = contourMap.begin(); it != contourMap.end(); ++it)
		{
			mc->SetValue(it->first,it->second);
		}
	}
}

void Volume::loadData(std::string filename)
{
	//Load volume data and create all VTK objects(currently only support MHD files)
	metaReader->SetFileName(filename.c_str());
	mc->SetInputConnection(metaReader->GetOutputPort());
	mc->ComputeNormalsOff();
	mc->ComputeGradientsOff();

	updateContour();

	confilter->SetInputConnection(mc->GetOutputPort());
	confilter->SetExtractionModeToLargestRegion();

	clippingPlane->SetOrigin(0,0,0);
	clippingPlane->SetNormal(0,0,-1);
	clipper->SetInputConnection(confilter->GetOutputPort());
	clipper->SetClipFunction(clippingPlane);
	clipper->GenerateClippedOutputOn();
	clipper->SetValue(0.5);
	
	
	updateClipping();
	mapper->ScalarVisibilityOff();

	actor->SetMapper(mapper);
	setColor(1,0,0);
}

//Update connections of mapper depending on clipping state
void Volume::updateClipping()
{
	if(isClippingEnabled)
	{
		mapper->SetInputConnection(clipper->GetOutputPort());
	}
	else
	{
		mapper->SetInputConnection(confilter->GetOutputPort());
	}
	
}

bool Volume::getClippingEnabled()
{
	return isClippingEnabled;
}
void Volume::setClippingEnabled(bool enabled)
{
	//Change clipping status here
	isClippingEnabled = enabled;
	updateClipping();

}

void Volume::setColor(double r, double g, double b)
{
	if(actor)
	{
		actor->GetProperty()->SetColor(r,g,b);
	}

}

void Volume::addSelfToMenu(Menu * menuToAddTo)
{
	//Add controls to menu



}


void Volume::onMenuItemEvent(MenuItem* mi)
{

	if(mi == isoSlider)
	{
		int value = mi->getSlider()->getValue();
		contourMap.at(0) = value;
		updateContour();
		std::cout << value << std::endl;

	}
	else if (mi == opacitySlider)
	{
		double value = mi->getSlider()->getValue()/1000.0;
		actor->GetProperty()->SetOpacity(value);
		cout << value << endl;
	}
	else if(mi == imageSlider)
	{
		int extent[6];
		int value = mi->getSlider()->getValue();
		imageActor->GetDisplayExtent(extent);
        extent[4] = value;
        extent[5] = value;
		imageActor->SetDisplayExtent(extent);

		cout << value << endl;
		//imageActor->SetZSlice(value);
		cout << imageActor->GetWholeZMax() << endl;

	}
}